const tolerances = {
    LOOSE_RUNNING: { hb_hole: 'H11', hb_shaft: 'c11', sb_hole: 'C11', sb_shaft: 'h11' },
    FREE_RUNNING: { hb_hole: 'H9', hb_shaft: 'd9', sb_hole: 'H9', sb_shaft: 'd9' },
    CLOSE_RUNNING: { hb_hole: 'H8', hb_shaft: 'f7', sb_hole: 'F8', sb_shaft: 'h7' },
    SLIDING: { hb_hole: 'H7', hb_shaft: 'g6', sb_hole: 'G7', sb_shaft: 'h6' },
    LOCATION: { hb_hole: 'H7', hb_shaft: 'h6', sb_hole: 'H7', sb_shaft: 'h6' },
    SIMILAR_FIT: { hb_hole: 'H7', hb_shaft: 'k6', sb_hole: 'K7', sb_shaft: 'h6' },
    FIXED_FIT: { hb_hole: 'H7', hb_shaft: 'n6', sb_hole: 'N7', sb_shaft: 'h6' },
    PRESS_FIT: { hb_hole: 'H7', hb_shaft: 'p6', sb_hole: 'P7', sb_shaft: 'h6' },
    DRIVING_FIT: { hb_hole: 'H7', hb_shaft: 's6', sb_hole: 'S7', sb_shaft: 'h6' },
    FORCED_FIT: { hb_hole: 'H7', hb_shaft: 'u6', sb_hole: 'U7', sb_shaft: 'h6' },
    // Add more colors as needed
};

async function parseCSV(){
    // Fetch the CSV file
    fetch('data.csv')
    .then(response => response.text())
    .then(csvData => {
        // Process the CSV data into an array of objects
        const rows = csvData.trim().split('\n');
        const header = rows[0].split(',').map(item => item.trim());
        const data = rows.slice(1).map(row => {
            const values = row.split(',').map(item => item.trim());
            const rowData = {};
            header.forEach((key, index) => {
                rowData[key] = values[index];
            });
            return rowData;
        });

        // Log the processed data
        console.log('Data:', data);

        // Example: Search for a specific value
        const searchResult = data.find(item => item.Name === 'John');
        console.log('Search Result:', searchResult);
    })
    .catch(error => console.error('Error fetching CSV:', error));

}

document.addEventListener('DOMContentLoaded', function () {
    // Add event listeners to the checkboxes
    var shaftCheckbox = document.getElementById('shaft');
    var holeCheckbox = document.getElementById('hole');
    var shaftLabel = document.getElementById('shaftLabel');
    var holeLabel = document.getElementById('holeLabel');

    shaftCheckbox.addEventListener('change', function () {
        toggleShaftInput();

        // If shaft checkbox is checked, hide hole checkbox and label
        holeCheckbox.style.display = shaftCheckbox.checked ? 'none' : 'block';
        holeLabel.style.display = shaftCheckbox.checked ? 'none' : 'block';
    });

    holeCheckbox.addEventListener('change', function () {
        toggleHoleInput();

        // If hole checkbox is checked, hide shaft checkbox and label
        shaftCheckbox.style.display = holeCheckbox.checked ? 'none' : 'block';
        shaftLabel.style.display = holeCheckbox.checked ? 'none' : 'block';
    });
});

function toggleShaftInput() {
    // Get the checkbox and subscribe input elements
    var shaftCheckbox = document.getElementById('shaft');
    var shaftInputContainer_min = document.getElementById('shaftInputContainer_min');
    var shaftInputContainer_max = document.getElementById('shaftInputContainer_max');

    // Toggle the display of the subscribe input based on the checkbox state
    shaftInputContainer_min.style.display = shaftCheckbox.checked ? 'block' : 'none';
    shaftInputContainer_max.style.display = shaftCheckbox.checked ? 'block' : 'none';
}

function toggleHoleInput() {
    // Get the checkbox and subscribe input elements
    var holeCheckbox = document.getElementById('hole');
    var holeInputContainer_min = document.getElementById('holeInputContainer_min');
    var holeInputContainer_max = document.getElementById('holeInputContainer_max');

    // Toggle the display of the subscribe input based on the checkbox state
    holeInputContainer_min.style.display = holeCheckbox.checked ? 'block' : 'none';
    holeInputContainer_max.style.display = holeCheckbox.checked ? 'block' : 'none';
}

function collectAndDisplay() {

    parseCSV();
    
    // Collect user inputs
    var dim = document.getElementById('dim').value;
    var shaftCheckbox = document.getElementById('shaft');
    var shaftInput_min = shaftCheckbox.checked ? document.getElementById('shaftInput_min').value : 'N/A';
    var shaftInput_max = shaftCheckbox.checked ? document.getElementById('shaftInput_max').value : 'N/A';
    var holeCheckbox = document.getElementById('hole');
    var holeInput_min = holeCheckbox.checked ? document.getElementById('holeInput_min').value : 'N/A';
    var holeInput_max = holeCheckbox.checked ? document.getElementById('holeInput_max').value : 'N/A';
    var fits = document.getElementById('fits').value;

    // Store the collected data
    storeUserData(dim, shaftCheckbox.checked, shaftInput_min, shaftInput_max, holeCheckbox.checked, holeInput_min, holeInput_max, fits);

    // Display the collected data
    displayUserData(dim, shaftCheckbox.checked, shaftInput_min, shaftInput_max, holeCheckbox.checked, holeInput_min, holeInput_max, fits);
}

function storeUserData(dim, OTSshaft, shaftInput_min, shaftInput_max, OTShole, holeInput_min, holeInput_max, fits) {
    // Store the data as needed, e.g., in an array, object, or send it to a server
    var userData = {
        dim: dim,
        OTSshaft: OTSshaft,
        shaftInput_min: OTSshaft ? shaftInput_min : 'N/A',
        shaftInput_max: OTSshaft ? shaftInput_max : 'N/A',
        OTShole: OTShole,
        holeInput_min: OTShole ? holeInput_min : 'N/A',
        holeInput_max: OTShole ? holeInput_max : 'N/A',
        fits: fits
    };

    console.log('Stored User Data:', userData);
}

function displayUserData(dim, OTSshaft, shaftInput_min, shaftInput_max, OTShole, holeInput_min, holeInput_max, fits) {
    // Access the display area in HTML
    var displayArea = document.getElementById('displayArea');

    const toleranceObject = tolerances[fits];

    if(OTShole == true){
        var content = `
        <h2>User Data:</h2>
        <p><strong>Nominal Dimension:</strong> ${dim}</p>
        <p><strong>OTS Shaft:</strong> ${OTSshaft ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Shaft Variation:</strong> ${OTSshaft ? shaftInput_min : 'N/A'}</p>
        <p><strong>Maximum Shaft Variation:</strong> ${OTSshaft ? shaftInput_max : 'N/A'}</p>
        <p><strong>OTS Hole:</strong> ${OTShole ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Hole Variation:</strong> ${OTShole ? holeInput_min : 'N/A'}</p>
        <p><strong>Maximum Hole Variation:</strong> ${OTShole ? holeInput_max : 'N/A'}</p>
        <p><strong>Fit:</strong> ${toleranceObject.hb_hole}/${toleranceObject.hb_shaft}</p>
    `;
    }
    else if(OTSshaft == true){
        var content = `
        <h2>User Data:</h2>
        <p><strong>Nominal Dimension:</strong> ${dim}</p>
        <p><strong>OTS Shaft:</strong> ${OTSshaft ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Shaft Variation:</strong> ${OTSshaft ? shaftInput_min : 'N/A'}</p>
        <p><strong>Maximum Shaft Variation:</strong> ${OTSshaft ? shaftInput_max : 'N/A'}</p>
        <p><strong>OTS Hole:</strong> ${OTShole ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Hole Variation:</strong> ${OTShole ? holeInput_min : 'N/A'}</p>
        <p><strong>Maximum Hole Variation:</strong> ${OTShole ? holeInput_max : 'N/A'}</p>
        <p><strong>Fit:</strong> ${toleranceObject.sb_hole}/${toleranceObject.sb_shaft}</p>
    `;
    }

    // Create HTML content to display user data
    

    // Set the content in the display area
    displayArea.innerHTML = content;
}


